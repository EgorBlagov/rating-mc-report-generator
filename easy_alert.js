const { remote } = require('electron');
const { BrowserWindow } = remote;
const url = require('url');
const path = require('path');
const { AppIcon } = require('./icon_provider');

function showAlert(message) {
    let alert = new AlertWindow(message);
    alert.show();
}

class AlertWindow {
    constructor(message) {
        this.message = message;
        this.dialog = new BrowserWindow({
            parent: remote.getCurrentWindow(),
            modal: true,
            width: 420,
            height: 210,
            resizable: false,
            frame: false,
            show: false,
            icon: AppIcon,            
        });

        this.dialog.webContents.on('did-finish-load', () => {
            this.dialog.webContents.send('alert:message', this.message);
            this.dialog.show();
        })
    }

    show() {
        this.dialog.loadURL(url.format({
            pathname: path.join(__dirname, 'alert.html'),
            protocol: 'file:',
            slashes:true
        }));
    }
}


module.exports = {
    showAlert : showAlert
}

