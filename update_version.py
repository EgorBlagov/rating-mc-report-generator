import re
import sys

def process_file(file_name, version, regexp):
    text = ''
    with open(file_name) as f:
        text = f.read()

    text = re.sub(regexp, r'\g<1>'+version+r'\g<2>', text, flags=re.M)

    with open(file_name, 'w') as f:
        f.write(text)

if len(sys.argv) < 2:
    raise ValueError('first argument must be a version')

new_version = sys.argv[1]

arguments = [
    ('package.json', r'("version" *: *").*(")'),
    ('inno/installation_script.iss', r'(MyAppVersion *").*(")'),
    ('inno/installation_script.iss', r'(OutputBaseFilename=RatingMC_RG_).*()'),
]

for file, reg in arguments:
    process_file(file, new_version, reg)
