const {app, BrowserWindow} = require('electron')
const path = require('path');
const url = require('url');
const AppIcon = require('./icon_provider');

let mainWindow;


// SET ENV
process.env.NODE_ENV = 'production';

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        width:480,
        height:550,
        resizable: false,
        frame: false,
        show: false,
        icon: AppIcon,
    });
    
    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.show();
    });

    mainWindow.on('closed', function(){
        app.quit();
    });

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'main.html'),
        protocol: 'file:',
        slashes:true
    }));
})
