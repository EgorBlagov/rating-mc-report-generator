const { remote } = require('electron');
const { BrowserWindow, ipcMain } = remote;
const { VK } = require('vk-io');
const url = require('url');
const path = require('path');
const yaml = require('node-yaml');
const { AppIcon } = require('./icon_provider');

const vk = new VK();
MAX_NEWSFEED_REQUREST_COUNT = 200;
MAX_GROUPSGET_REQUEST_COUNT = 500;

class Config {
    constructor() {
        this.config_filename = path.join(process.cwd(), "config.yaml");

        this.config = {};

        this.config_defaults = {
            APP: "5892520",
        }

        this.applyDefaults();

        try {
            let readen = yaml.readSync(this.config_filename)
            for (const item in readen) {
                this.config[item] = readen[item];
            }
        } catch (error) {
        }
        this.save();
    }

    applyDefaults() {
        for (const item in this.config_defaults) {
            this.config[item] = this.config_defaults[item];
        }
    }

    get(key) {
        return this.config[key];
    }

    set(key, val) {
        this.config[key] = val;
        this.save();
    }

    has(key) {
        return this.config.hasOwnProperty(key);
    }

    remove(key) {
        delete this.config[key];
        this.save();
    }

    save() {
        try {
            yaml.writeSync(this.config_filename, this.config);
        } catch (error) {
        }
    }
}

let cfg = new Config();


vk.captchaHandler = (params, callback) => {
    let captchaDialog = new BrowserWindow({
        parent: remote.getCurrentWindow(),
        modal: true,
        width: 250,
        height: 220,
        resizable: false,
        frame: false,
        show: false,
        icon: AppIcon,
    });

    captchaDialog.webContents.on('did-finish-load', () => {
        captchaDialog.webContents.send('captcha:image', params.src);
        captchaDialog.show();
    });

    captchaDialog.loadURL(url.format({
        pathname: path.join(__dirname, 'captcha.html'),
        protocol: 'file:',
        slashes: true
    }));

    ipcMain.on('captcha:code', (e, code) => {
        callback(code);
    });

    captchaDialog.webContents.on('close', () => {
        callback(new Error("Необходимо ввести код"));
    });
}

class InterruptedException extends Error {
    constructor() {
        super("Операция прервана пользователем");
        this.name = this.constructor.name;
    }

    toString() {
        return this.name + ": " + this.message;
    }
}

class AppCore {
    constructor(listParent, progressbar, status) {
        this.data = null;
        this.itemList = new ItemList(listParent);
        this.authorized = false;
        this.progressManager = new ProgressManager(progressbar);
        this.status = status;
        this.interrupted = false;
        this.savePassIfSuccess = false;
    }

    async authorize() {
        this.setStatus("Проверка сохраненного токена")
        let isAlreadyAuthorized = await this.checkIsAuthorized()
        if (!isAlreadyAuthorized) {
            await this.enterLoginAndPass();
            vk.setOptions({
                app: cfg.get('APP'),
            });
            const implicitFlow = vk.auth.implicitFlowUser();

            let response = await implicitFlow.run();

            if (this.savePassIfSuccess) {
                cfg.set('LOGIN', this.savedLogin);
                cfg.set('PASSWORD', this.savedPass);
            } else {
                cfg.remove('LOGIN');
                cfg.remove('PASSWORD');
            }

            if (this.saveTokenIfSuccess) {
                cfg.set('TOKEN', response.token);
            }
            vk.setOptions({
                token: response.token,
            });
        }

        this.authorized = true;
        this.setStatus("Готов к поиску")
    }

    async enterLoginAndPass() {
        this.setStatus("Введите логин и пароль")
        let loginDialog = new BrowserWindow({
            parent: remote.getCurrentWindow(),
            modal: true,
            width: 400,
            height: 275,
            resizable: false,
            frame: false,
            show: false,
            icon: AppIcon,
        });

        loginDialog.webContents.on('did-finish-load', () => {
            loginDialog.show();
            if (cfg.has('LOGIN') && cfg.has('PASSWORD')) {
                loginDialog.webContents.send('login:saved', cfg.get('LOGIN'), cfg.get('PASSWORD'));
            }
        });

        loginDialog.loadURL(url.format({
            pathname: path.join(__dirname, 'login.html'),
            protocol: 'file:',
            slashes: true
        }));

        return new Promise((resolve, reject) => {
            const userCancelMessage = "Авторизация отменена пользователем";
            ipcMain.on('login:enter', (e, login, password, saveData, saveToken) => {
                vk.setOptions({
                    login: login,
                    password: password,
                })
                this.savePassIfSuccess = saveData;
                this.saveTokenIfSuccess = saveToken;
                this.savedLogin = login;
                this.savedPass = password;
                resolve();
            });

            loginDialog.on('closed', (e) => {
                reject(userCancelMessage);
            })
        });
    }

    async checkIsAuthorized() {
        if (!cfg.has('TOKEN')) {
            return false;
        }
        try {
            vk.setOptions({
                token: cfg.get('TOKEN')
            });

            let parameters = {
                app_id: cfg.get('APP'),
            };

            let response = await vk.api.apps.get(parameters);

            return response.items[0].id == cfg.get('APP');
        } catch (error) {
            return false;
        }
    }

    isAuthorized() {
        return this.authorized;
    }

    isTokenSaved() {
        return cfg.has('TOKEN');
    }

    resetToken() {
        cfg.remove('TOKEN');
    }

    async sendRequest() {
        this.interrupted = false;
        try {
            let responseParser = new ResponseParser();

            this.progressManager.startItemSearch(this.getItemList().getItems());

            for (const item of this.getItemList().getItems()) {
                this.setStatus("Поиск по фразе \"" + item.value + "\"");
                await this.searchItem(item, responseParser);
                this.progressManager.nextItemSearch();
            };

            this.setStatus("Запрос данных о подписчиках");

            await this.getGroupsInfo(responseParser);

            this.data = responseParser.getData();
            this.progressManager.completeSearch();

            this.setStatus("Всего получено " + this.data.length + " результатов");
        } catch (exception) {
            if (exception instanceof InterruptedException) {
                this.setStatus("Операция прервана");
                this.progressManager.reset();
                this.data = null;
            } else {
                throw exception
            }
        }
    }

    stopRequest() {
        this.interrupted = true;
    }

    cancelIfInterrupted() {
        if (this.interrupted) {
            throw new InterruptedException();
        }
    }

    async searchItem(item, responseParser) {
        let start_from = null;
        while (true) {
            let parameters = {
                q: item.value,
                count: MAX_NEWSFEED_REQUREST_COUNT,
                start_from: start_from,
                extended: true,
                fields: 'followers_count',
            };

            this.cancelIfInterrupted();

            let response = await vk.api.newsfeed.search(parameters);
            this.progressManager.pushNewsfeedResponse(response);
            responseParser.pushNewsfeedResponse(response);
            start_from = response.next_from;
            if (start_from == null) {
                break;
            }
        }
    }

    async getGroupsInfo(responseParser) {
        function distinctById(value, index, self) {
            return self.findIndex(el => el.id == value.id) === index;
        }
        let unique_groups_ids = responseParser.groups.filter(distinctById).map(gr => gr.id);
        for (let i = 0; i < unique_groups_ids.length; i += MAX_GROUPSGET_REQUEST_COUNT) {
            let bunch_groups = unique_groups_ids.slice(i, i + MAX_GROUPSGET_REQUEST_COUNT)
            let parameters = {
                group_ids: bunch_groups.join(','),
                fields: 'members_count',
            };

            this.cancelIfInterrupted();

            let response = await vk.api.groups.getById(parameters);
            responseParser.pushGroupsGetResponse(response);
        }
    }

    isDataReceived() {
        return this.data != null;
    }

    getItemList() {
        return this.itemList;
    }

    exportReport(remove_post_duplicates, remove_group_duplicates, request_data, date_info) {
        if (this.data == null) {
            throw new Error("Невозможно сформировать отчет: нет данных. Для начала выполните запрос");
        }

        let reportGenerator = new ReportGenerator(
            this.data,
            this.getItemList().getItems(),
            remove_post_duplicates,
            remove_group_duplicates,
            request_data,
            date_info,
        );

        let contents = reportGenerator.generate();

        let previewDialog = new BrowserWindow({
            parent: remote.getCurrentWindow(),
            frame: false,
            show: false,
            modal: true,
            icon: AppIcon,
        });

        previewDialog.webContents.on('did-finish-load', () => {
            previewDialog.webContents.send('report:content', contents);
            previewDialog.show();
        });

        previewDialog.loadURL(url.format({
            pathname: path.join(__dirname, 'report_preview.html'),
            protocol: 'file:',
            slashes: true
        }));
    }

    setStatus(status) {
        this.status.innerHTML = status;
    }
}

class ProgressManager {
    constructor(progressbar) {
        this.progressbar = progressbar;
        this.reset();
    }

    startItemSearch(items) {
        this.setActive(true);
        this.progressForItem = 1.0 / items.length;

        this.currentItemStartProgress = 0;

        this.resetCurrentItemProgress();
        this.setProgress(this.getProgressForCurrentItemAndCollected());
    }

    reset() {
        this.progressForItem = 0;
        this.currentItemStartProgress = 0;
        this.setActive(false);
        this.setProgress(0);
        this.resetCurrentItemProgress();
    }

    resetCurrentItemProgress() {
        this.currentItemCount = 0;
        this.currentItemCollected = 0;
    }

    getProgressForCurrentItemAndCollected() {
        let currentItemProgress = this.currentItemCount == 0 ? 0 : this.currentItemCollected / this.currentItemCount;
        return this.currentItemStartProgress + currentItemProgress * this.progressForItem;
    }

    nextItemSearch() {
        this.currentItemStartProgress += this.progressForItem;
        this.resetCurrentItemProgress();
        this.setProgress(this.getProgressForCurrentItemAndCollected());
    }

    pushNewsfeedResponse(response) {
        if (this.currentItemCount == 0) {
            this.currentItemCount = response.count;
        }
        this.currentItemCollected += response.items.length;
        this.setProgress(this.getProgressForCurrentItemAndCollected());
    }

    completeSearch() {
        this.setProgress(1.0);
        this.setActive(false);
    }

    setProgress(fraction) {
        this.currentProgress = fraction;
        this.updateProgressBar();
    }

    setActive(active) {
        this.active = active;
        this.updateProgressBar();
    }

    updateProgressBar() {
        let percents = Math.round(this.currentProgress * 100);
        this.progressbar.setAttribute("aria-valuenow", percents);
        this.progressbar.style.width = percents + "%";

        this.progressbar.classList.toggle("progress-bar-animated", this.active);
    }
}

class ItemList {
    constructor(parentElement) {
        this.items = [];
        this.parentElement = parentElement;
    }

    addItem(item) {
        if (!this.items.find(el => el.getValue() == item)) {
            item = new Item(item, this);
            this.items.push(item);
        }
        this.renderItems();
    }

    removeSelected() {
        this.items = this.items.filter(el => !el.isSelected());
        this.renderItems();
    }

    clearItems() {
        this.items = [];
        this.renderItems();
    }

    getItems() {
        return this.items;
    }

    renderItems() {
        this.parentElement.innerHTML = '';
        this.items.forEach(el => {
            el.render(ul)
        })
    }

    clearSelection() {
        this.items.forEach(el => {
            el.select(false);
        })
    }
}

class Item {
    constructor(value, list) {
        this.value = value;
        this.selected = false;
        this.list = list;
    }

    select(selected) {
        this.selected = selected;
    }

    isSelected() {
        return this.selected;
    }

    getValue() {
        return this.value;
    }

    render(parent) {
        const doc = parent.ownerDocument;
        const li = doc.createElement('li');
        li.classList.add('list-group-item');
        li.addEventListener('click', this.onClick.bind(this));
        const itemText = doc.createTextNode(this.value);
        if (this.isSelected()) {
            li.classList.add('active');
        }
        li.appendChild(itemText);
        parent.appendChild(li);
    }

    onClick(event) {
        if (!event.shiftKey) {
            this.list.clearSelection();
        }

        this.select(true);
        this.list.renderItems();
    }
}

class ResponseParser {
    constructor() {
        this.items = [];
        this.groups = [];
        this.profiles = [];
        this.data = null;
    }

    pushNewsfeedResponse(response) {
        this.items = this.items.concat(response.items);
        this.groups = this.groups.concat(response.groups);
        this.profiles = this.profiles.concat(response.profiles);
    }

    pushGroupsGetResponse(response) {
        for (const group of this.groups) {
            if (group.members_count == null) {
                let group_desc = response.find(gr => gr.id == group.id);
                if (group_desc != null) {
                    group.members_count = group_desc.members_count;
                }
            }
        }
    }

    getData() {
        if (this.data == null) {
            this.parse();
        }
        return this.data;
    }

    parse() {
        this.data = [];
        for (const item of this.items) {
            this.data.push(new ResponseItem(item, this.groups, this.profiles));
        }
    }
}

class ResponseItem {
    constructor(item, groups, profiles) {
        this.fields = [];
        this.id = item.id
        this.owner_id = item.owner_id;
        this.likes = item.likes == null ? 0 : item.likes.count;
        this.reposts = item.reposts == null ? 0 : item.reposts.count;
        this.views = item.views == null ? 0 : item.views.count;
        this.type = this.owner_id < 0 ? "Сообщество" : "Пользователь";
        this.owner_name = this.getOwnerName(this.owner_id, groups, profiles);
        this.owner_url = "https://vk.com/" + (this.owner_id < 0 ? "club" : "id") + Math.abs(this.owner_id);
        this.post_url = "https://vk.com/wall" + this.owner_id + '_' + this.id;
        this.followers = this.getOwnerFollowers(this.owner_id, groups, profiles);

        this.addField('Тип страницы', this.type);
        this.addField('Название (Имя)', this.owner_name);
        this.addField('Количество подписчиков', this.followers);
        this.addField('URL страницы', this.owner_url);
        this.addField('URL поста', this.post_url);
        this.addField('Лайки', this.likes);
        this.addField('Репосты', this.reposts);
        this.addField('Просмотры', this.views);
    }

    getOwnerName(owner_id, groups, profiles) {
        if (owner_id < 0) {
            return groups.find(gr => gr.id == -owner_id).name;
        } else {
            let user = profiles.find(usr => usr.id == owner_id);
            return user.first_name + ' ' + user.last_name;
        }
    }

    getOwnerFollowers(owner_id, groups, profiles) {
        if (owner_id < 0) {
            return groups.find(gr => gr.id == -owner_id).members_count;
        } else {
            let user = profiles.find(usr => usr.id == owner_id);
            return user.followers_count;
        }
    }

    addField(fieldName, fieldValue) {
        this.fields.push({
            'name': fieldName,
            'value': fieldValue
        });
    }
}

class ReportGenerator {
    constructor(data, items, remove_post_duplicates, remove_group_duplicates, insert_request_data, insert_date_info) {
        this.data = data;
        this.searched_items = items;
        this.remove_post_duplicates = remove_post_duplicates;
        this.remove_group_duplicates = remove_group_duplicates;
        this.insert_request_data = insert_request_data;
        this.insert_date_info = insert_date_info;

        if (this.remove_post_duplicates) {
            this.removePostDuplicates();
        }
        if (this.remove_group_duplicates) {
            this.removeGroupDuplicates();
        }
    }

    removePostDuplicates() {
        this.data = this.data.filter((value, index, self) => {
            return self.findIndex(el => el.post_url == value.post_url) === index;
        });
    }

    removeGroupDuplicates() {
        this.data = this.data.filter((value, index, self) => {
            return self.findIndex(el => el.owner_url == value.owner_url) === index;
        });
    }

    generate() {
        let request_data = this.generateRequestData();
        let date_info = this.generateDateInfo();
        let first = true;

        let totalLikes = 0;
        let totalReposts = 0;
        let totalViews = 0;
        let totalFollowers = 0;
        let table = "<table>";
        for (const item of this.data) {
            if (first) {
                first = false;
                table += "<tr>" + this.headerRow(item) + "</tr>";
            }
            table += "<tr>" + this.row(item) + "</tr>";
            totalLikes += item.likes;
            totalReposts += item.reposts;
            totalViews += item.views;
            totalFollowers += item.followers;
        }
        table += "</table>";
        let bottom = "<div>" + "Лайки: " + totalLikes + ", Репосты: " + totalReposts + ", Просмотры: " + totalViews + ", Подписчики: " + totalFollowers + "." + "</div>";
        return `<!DOCTYPE html>
                <html>
                    ${this.generateHeader()}
                    <body>
                    ${request_data + table + bottom + date_info}
                    </body>
                </html>`;
    }

    generateRequestData() {
        if (!this.insert_request_data) {
            return "";
        }

        let lis = "";
        for (const item of this.searched_items) {
            lis += `<li>${item.value}</li>`;
        }

        let duplicate_remove_data = "";

        if (this.remove_post_duplicates) {
            duplicate_remove_data += "<div>Удалены дубликаты постов</div>";
        }

        if (this.remove_group_duplicates) {
            duplicate_remove_data += "<div>Удалены дубликаты групп (пользователей)</div>";
        }

        return `<div>
                    <div>
                        На основании запросов:
                    </div>
                    <ul>
                        ${lis}
                    </ul>
                    ${duplicate_remove_data}
                </div>`;
    }

    generateDateInfo() {
        if (!this.insert_date_info) {
            return "";
        }

        function two(value) {
            return ("0" + value).slice(-2)
        }

        let date_now = new Date();
        let date_str = `${two(date_now.getDate())}.${two(date_now.getMonth())}.${date_now.getFullYear()}`;
        let date_info = "Отчет сгенерирован: " + date_str

        return `<div class="date-info">
                    ${date_info}
                </div>`;
    }

    generateHeader() {
        let style = ` <style>
        table {
            border-collapse: collapse;
            font-size: small;
        }

        th {
            background: rgb(246, 255, 161);
        }
        
        table, td, th {
            border: 1px solid black;
            padding: 5px;
        }

        .date-info {
            color: #aaa;
        }
        </style>`;

        return `<head><title>Report</title><meta charset=\"UTF-8\">${style}</head>`;
    }

    headerRow(item) {
        let headerCells = item.fields.map(el => el.name);
        let result = '';
        for (const cell of headerCells) {
            result += this.headerCell(cell);
        }
        return result;
    }

    row(item) {
        let cells = item.fields.map(el => el.value);
        let result = '';
        for (const cell of cells) {
            let string_cell = String(cell);
            if (string_cell.startsWith('https')) {
                string_cell = `<a href="${string_cell}" target="_blank">${string_cell}</a>`;
            }
            result += this.cell(string_cell);
        }
        return result;
    }

    cell(data) {
        return "<td>" + data + "</td>";
    }

    headerCell(data) {
        return "<th>" + data + "</th>";
    }
}

module.exports = {
    AppCore: AppCore
}