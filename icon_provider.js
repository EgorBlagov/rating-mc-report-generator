const { nativeImage } = require('electron');
const path = require('path');
let icon_path = path.join(__dirname, 'AppIcon.ico');
let AppIcon = nativeImage.createFromPath(icon_path);

module.exports = {
    AppIcon : icon_path,
}